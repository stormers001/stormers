<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'storme_documentation');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's*:X|Gfd7%XvY^>7WKeaJdY)9-~UW/!XoOJd/KyHe6CU@&*aSyI1I{;XCX-R -z7');
define('SECURE_AUTH_KEY',  'CO!,0HELU& ,8>tMV MmX9M :7_+y@):0&0klkfEhgqcsR^#rO<y%V9,r`X93V(-');
define('LOGGED_IN_KEY',    'nA9(VCQ74p^Yhe@^Yut9w:#icC!h_~G|A4kW *.%ow:$F&2iCbDlb coOp{~Qz;o');
define('NONCE_KEY',        'if7t)2VY8Y~[]jn7hcQ|7&2?Ll#bshaGB8p/[I51Sv;#?iv7H^z=JM5;kgHGf6*5');
define('AUTH_SALT',        'Tdb$q,-wFf{hKup8HvT7I}[-iW4~Kz{toda}|ao:bm,7(@fY hU%9[m$$tblJT-3');
define('SECURE_AUTH_SALT', '7 IBXXM?B@iR;91=i1#Pj)CrAxb7~6V1#(S`[0hcC+D)/b.r,2Iv`B,%A9_z9p&9');
define('LOGGED_IN_SALT',   '9 &LskJZH5>x1}dcpGuVvgd (%w KmK[L^+u8?}+EDJ{UW$!uv[$DAme<YO,|opJ');
define('NONCE_SALT',       '|?un@:LGzS$%`.g-m@9a^-:38Z>=%./KXISe2>oB;yclKDPN|^zg;56Nv[4cYP]G');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
